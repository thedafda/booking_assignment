module.exports = function(config) {
  config.set({
    basePath: '.',
    frameworks: ['jasmine'],
    files: [
      'public/scripts/utils/**/*.js',
      'public/scripts/ui_components/**/*.js',
      'public/scripts/models/**/*.js',
      'spec/unit/**/*.js'
    ],
    reporters: ['mocha'],
    browsers: ['PhantomJS', 'Chrome', 'Firefox'], //PhantomJS,Chrome,ChromeCanary
    singleRun: true
  });
};