/* jshint expr: true */
describe('Integration Tests', function() {

    before(function(){
        var chai = require('chai');
        var chaiAsPromised = require('chai-as-promised');
     
        chai.use(chaiAsPromised);
        expect = chai.expect;
        chai.Should();
        return browser
            .url('http://localhost:9001');
    });

    describe('Hotel Page', function(){


        describe('location map', function () {
            
            it('map container should exist', function(){
                return browser
                    .element('#map-canvas')
                    .then(function(res){
                        return browser.elementIdDisplayed(res.value.ELEMENT);
                    })
                    .then(function(res){
                        res.value.should.be.true;
                    });
            });
        });
        
        describe('rooms list', function () {
            
            it('rooms table should exist', function(){
                return browser
                    .element('#rooms-table')
                    .then(function(res){
                        return browser.elementIdDisplayed(res.value.ELEMENT);
                    })
                    .then(function(res){
                        res.value.should.be.true;
                    });
            });

            it('rooms table should have three rows: Room Name, Occupancy, Price Per room and No. Rooms', function(){
                return browser
                    .elements('#rooms-table thead tr th')
                    .then(function(res){
                        res.value.length.should.equal(4);
                    });
            });

            it('on clicking on Price Per room title, column should get sorted ascending order', function(){
                return browser
                    .elements('#rooms-table thead tr th')
                    .then(function(res){
                        // clicking third column
                        return browser.elementIdClick(res.value[2].ELEMENT);
                    })
                    .getText('#rooms-table tbody tr .room_price')
                    .then(function(textArr){
                        var last;
                        var current;
                        for(var i = 1; i < textArr.length - 1; i++){
                            last = Number(textArr[i - 1].replace('€',''));
                            current = Number(textArr[i].replace('€',''));
                            last.should.be.above(current);
                        }
                    });
            });

            it('on clicking on Price Per room title again, column should get sorted descending order', function(){
                return browser
                    .elements('#rooms-table thead tr th')
                    .then(function(res){
                        // clicking third column again
                        return browser.elementIdClick(res.value[2].ELEMENT);
                    })
                    .getText('#rooms-table tbody tr .room_price')
                    .then(function(textArr){
                        var last;
                        var current;
                        for(var i = 1; i < textArr.length - 1; i++){
                            last = Number(textArr[i - 1].replace('€',''));
                            current = Number(textArr[i].replace('€',''));
                            last.should.be.below(current);
                        }
                    });
            });

        });

        describe('related hotels list', function () {
            
            it('related hotels list exist should be visible', function(){
                return browser
                .element('#related-hotels-container')
                .then(function(res){
                    return browser.elementIdDisplayed(res.value.ELEMENT);
                })
                .then(function(res){
                    res.value.should.be.true;
                });
                
            });

            it('related hotels section should have 4 hotels', function(){
                return browser
                .elements('#related-hotels-container .related-hotel')
                .then(function(res){
                    res.value.length.should.equal(4);
                });
            });

            it('hotel section should have title, image, description', function(){
                return browser
                .element('#related-hotels-container .related-hotel')
                .then(function(res){
                    expect(res.value.ELEMENT).to.not.be.undefined;
                });
            });
            
        });

        describe('reviews', function(){
            it('total number reviews', function () {
                return browser
                .getText('#total-reviews')
                .then(function(ele){
                    var number = ele.split('(')[1].replace(')','');
                    number.should.equal('29');
                });
            });

            it('by default first page should be selected', function () {
                return browser
                .getAttribute('#reviews-paging-container .pagination [pages] div', 'class')
                .then(function(arr){
                    arr[0].should.equal('active');
                });
            });

            it('clicking on second page', function () {
                return browser
                .elements('#reviews-paging-container .pagination [pages] div')
                .then(function(res){
                    // clicking the second page
                    return browser.elementIdClick(res.value[1].ELEMENT); 
                })
                .getAttribute('#reviews-paging-container .pagination [pages] div', 'class')
                .then(function(pageList){
                    pageList[0].should.not.contain('active');
                    pageList[1].should.contain('active');
                    pageList[2].should.not.contain('active');
                });
            });


            it('clicking on next button', function () {
                return browser
                .element('#reviews-paging-container .pagination [value="next"]')
                .then(function(res){
                    // clicking the next button
                    return browser.elementIdClick(res.value.ELEMENT); 
                })
                .getAttribute('#reviews-paging-container .pagination [pages] div', 'class')
                .then(function(pageList){
                    pageList[0].should.not.contain('active');
                    pageList[1].should.not.contain('active');
                    pageList[2].should.contain('active');
                    pageList[3].should.not.contain('active');
                });
            });

            it('clicking on previous button', function () {
                return browser
                .element('#reviews-paging-container .pagination [value="prev"]')
                .then(function(res){
                    // clicking the next button
                    return browser.elementIdClick(res.value.ELEMENT); 
                })
                .getAttribute('#reviews-paging-container .pagination [pages] div', 'class')
                .then(function(pageList){
                    pageList[0].should.not.contain('active');
                    pageList[1].should.contain('active');
                    pageList[2].should.not.contain('active');
                    pageList[3].should.not.contain('active');
                });
            });


            it('clicking on end button', function () {
                return browser
                .element('#reviews-paging-container .pagination [value="end"]')
                .then(function(res){
                    // clicking the next button
                    return browser.elementIdClick(res.value.ELEMENT); 
                })
                .getAttribute('#reviews-paging-container .pagination [pages] div', 'class')
                .then(function(pageList){
                    pageList[0].should.not.contain('active');
                    pageList[1].should.not.contain('active');
                    pageList[2].should.not.contain('active');
                    pageList[3].should.not.contain('active');
                    pageList[pageList.length - 1].should.contain('active');
                });
            });

            it('clicking on start button', function () {
                return browser
                .element('#reviews-paging-container .pagination [value="start"]')
                .then(function(res){
                    // clicking the next button
                    return browser.elementIdClick(res.value.ELEMENT); 
                })
                .getAttribute('#reviews-paging-container .pagination [pages] div', 'class')
                .then(function(pageList){
                    pageList[0].should.contain('active');
                    pageList[1].should.not.contain('active');
                    pageList[2].should.not.contain('active');
                    pageList[3].should.not.contain('active');
                    pageList[pageList.length - 1].should.not.contain('active');
                });
            });
        });
        

    });
  
});
/* jshint expr: false */