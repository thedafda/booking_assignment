describe('models.reviews',function(){

	beforeEach(function(){
		this.dummyData = [
			1,2,3,4,5,6,7,8,'A','B','C','D','E','I','II','III'
		];
	});

	it('review model with default values', function(){
		var rm = new ReviewsModel(this.dummyData);
		expect(rm.getTotalReviews()).toBe(this.dummyData.length);
		expect(rm.getPage(0)).toEqual([1,2,3,4,5]);
		expect(rm.getPage(1)).toEqual([6,7,8,'A','B']);
	});

	it('review model with custom values', function(){
		var rm = new ReviewsModel(this.dummyData, {
			itemsPerPage: 8
		});
		expect(rm.getTotalReviews()).toBe(this.dummyData.length);
		expect(rm.getPage(0)).toEqual([1,2,3,4,5,6,7,8]);
		expect(rm.getPage(1)).toEqual(['A','B','C','D','E','I','II','III']);
	});

	it('checking for valid page number', function(){
		var rm = new ReviewsModel(this.dummyData);
		expect(rm.isValidPageNumber(0)).toBeTruthy();
		expect(rm.isValidPageNumber(1)).toBeTruthy();
		expect(rm.isValidPageNumber(2)).toBeTruthy();
		expect(rm.isValidPageNumber(3)).toBeTruthy();
		expect(rm.isValidPageNumber(4)).toBeFalsy();
		expect(rm.isValidPageNumber(-2)).toBeFalsy();
		expect(rm.isValidPageNumber(20)).toBeFalsy();
		expect(rm.isValidPageNumber('GARBAGE_VALUE')).toBeFalsy();
		expect(rm.isValidPageNumber({})).toBeFalsy();
	});

	it('should get next page', function(){
		var rm = new ReviewsModel(this.dummyData);
		expect(rm.getNextPage()).toEqual([6,7,8,'A','B']);
	});

	it('should get previous page', function(){
		var rm = new ReviewsModel(this.dummyData);
		expect(rm.getNextPage()).toEqual([6,7,8,'A','B']);
		expect(rm.getPreviousPage()).toEqual([1,2,3,4,5]);
	});

	it('should get first page', function(){
		var rm = new ReviewsModel(this.dummyData);
		expect(rm.getFirstPage()).toEqual([1,2,3,4,5]);
	});

	it('should get last page', function(){
		var rm = new ReviewsModel(this.dummyData);
		expect(rm.getLastPage()).toEqual(['III']);
	});

});