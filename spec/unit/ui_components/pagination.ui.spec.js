describe('ui.pagination',function(){

	beforeEach(function(){

		this.dummyArray = [
			{
				name: 'John',
				age: 89,
			},
			{
				name: 'Robert',
				age: 43,
			},
			{
				name: 'Jigar',
				age: 23,
			},
			{
				name: 'Sumit',
				age: 18,
			},
			{
				name: 'Ashutosh',
				age: 20,
			},
			{
				name: 'Kinjal',
				age: 24,
			},
			{
				name: 'Neel',
				age: 27,
			},
			{
				name: 'Akash',
				age: 34,
			},
			{
				name: 'Ashok',
				age: 30,
			},
			{
				name: 'Nikon',
				age: 33,
			},
			{
				name: 'Ravi',
				age: 29,
			},
			{
				name: 'Hitesh',
				age: 24,
			},
			{
				name: 'Chetan',
				age: 50,
			},
			{
				name: 'Jack',
				age: 65,
			}
 		];
 		
		this.testContainer = document.createElement('div');
		this.testContainer.innerHTML = [
		  		'<div id="list"></div>',
		  		'<div id="pages"></div>',
		].join('');
		this.listContainer = this.testContainer.querySelector('#list');
		this.pagesContainer = this.testContainer.querySelector('#pages');
		document.body.appendChild(this.testContainer);
		this.clickEvent = document.createEvent("MouseEvents");
		this.clickEvent.initEvent("click", true, true);
	});

	afterEach(function(){
		document.body.removeChild(this.testContainer);
	});


	describe('default values', function(){
		var row = [
		  	'<div>',
		  		'<div>{score}</div>',
		  		'<div>{age}</div>',
		  	'</div>'
		].join('');

		beforeEach(function(){
			this.rm = new ReviewsModel(this.dummyArray);
			this.pg = new window.UI.Pagination(this.rm, {
		  		container: this.listContainer,
		  		pagingContainer: this.pagesContainer,
		  		render: function(rowObject){
		  			return row.supplant(rowObject);
		  		}
			});
		});

		it('Should render 5(default value) items of first page', function(){
			
			expect(this.listContainer.children.length).toEqual(5);

			var numberOfPages = Math.ceil(this.dummyArray.length / 5);
			var pageDivs = this.pagesContainer.querySelector('[pages]');
			expect(pageDivs.children.length).toEqual(numberOfPages);

			expect(pageDivs.children[0].getAttribute('class')).toContain('active');
		});

		it('Should render 5(default value) items of second page', function(){
				
			this.pg.renderPage(1);
			
			expect(this.listContainer.children.length).toEqual(5);

			var pageDivs = this.pagesContainer.querySelector('[pages]');
			var numberOfPages = Math.ceil(this.dummyArray.length / 5);
			expect(pageDivs.children.length).toEqual(numberOfPages);

			expect(pageDivs.children[0].getAttribute('class')).not.toContain('active');
			expect(pageDivs.children[1].getAttribute('class')).toContain('active');

		});

		it('clicking on next should go to next page', function(){

			this.pagesContainer.querySelector('[value="next"]').dispatchEvent(this.clickEvent);
			
			expect(this.listContainer.children.length).toEqual(5);

			var pageDivs = this.pagesContainer.querySelector('[pages]');
			var numberOfPages = Math.ceil(this.dummyArray.length / 5);
			expect(pageDivs.children.length).toEqual(numberOfPages);

			expect(pageDivs.children[0].getAttribute('class')).not.toContain('active');
			expect(pageDivs.children[1].getAttribute('class')).toContain('active');

		});

		it('clicking on previous should go to previous page', function(){

			this.pagesContainer.querySelector('[value="next"]').dispatchEvent(this.clickEvent);
			this.pagesContainer.querySelector('[value="prev"]').dispatchEvent(this.clickEvent);
			
			expect(this.listContainer.children.length).toEqual(5);

			var pageDivs = this.pagesContainer.querySelector('[pages]');
			var numberOfPages = Math.ceil(this.dummyArray.length / 5);
			expect(pageDivs.children.length).toEqual(numberOfPages);

			expect(pageDivs.children[0].getAttribute('class')).toContain('active');
			expect(pageDivs.children[1].getAttribute('class')).not.toContain('active');

		});

		it('clicking on end should go to end page', function(){

			this.pagesContainer.querySelector('[value="end"]').dispatchEvent(this.clickEvent);
			
			expect(this.listContainer.children.length).toEqual(4);

			var pageDivs = this.pagesContainer.querySelector('[pages]');
			var numberOfPages = Math.ceil(this.dummyArray.length / 5);
			expect(pageDivs.children.length).toEqual(numberOfPages);

			expect(pageDivs.children[0].getAttribute('class')).not.toContain('active');
			expect(pageDivs.children[1].getAttribute('class')).not.toContain('active');
			expect(pageDivs.children[pageDivs.children.length - 1].getAttribute('class')).toContain('active');

		});

		it('clicking on start should go to start page', function(){

			this.pagesContainer.querySelector('[value="end"]').dispatchEvent(this.clickEvent);
			
			expect(this.listContainer.children.length).toEqual(4);

			var pageDivs = this.pagesContainer.querySelector('[pages]');
			var numberOfPages = Math.ceil(this.dummyArray.length / 5);
			expect(pageDivs.children.length).toEqual(numberOfPages);

			expect(pageDivs.children[0].getAttribute('class')).not.toContain('active');
			expect(pageDivs.children[1].getAttribute('class')).not.toContain('active');
			expect(pageDivs.children[pageDivs.children.length - 1].getAttribute('class')).toContain('active');

			this.pagesContainer.querySelector('[value="start"]').dispatchEvent(this.clickEvent);
			
			expect(this.listContainer.children.length).toEqual(5);

			pageDivs = this.pagesContainer.querySelector('[pages]');
			numberOfPages = Math.ceil(this.dummyArray.length / 5);
			expect(pageDivs.children.length).toEqual(numberOfPages);

			expect(pageDivs.children[0].getAttribute('class')).toContain('active');
			expect(pageDivs.children[1].getAttribute('class')).not.toContain('active');
			expect(pageDivs.children[pageDivs.children.length - 1].getAttribute('class')).not.toContain('active');

		});

	});

});