describe('ui.tablesorter',function(){

	beforeEach(function(){
		var testTmplt = [
		"<table>",
  			"<thead>",
				"<tr>",
      				"<th>Month</th>",
      				"<th>Savings</th>",
    			"</tr>",
  			"</thead>",
			"<tbody>",
    			"<tr>",
      				"<td>January</td>",
      				"<td>$800</td>",
    			"</tr>",
    			"<tr>",
      				"<td>February</td>",
      				"<td>$400</td>",
    			"</tr>",
    			"<tr>",
      				"<td>March</td>",
      				"<td>$600</td>",
    			"</tr>",
    			"<tr>",
      				"<td>April</td>",
      				"<td>$300</td>",
    			"</tr>",
    		 "</tbody>",
		"</table>"].join('');
 
		this.testContainer = document.createElement('div');
		this.testContainer.innerHTML = testTmplt;
		this.head = this.testContainer.querySelector('thead').children[0].children;
		this.rows = this.testContainer.querySelector('tbody').children;
		document.body.appendChild(this.testContainer);
	});

	afterEach(function(){
		document.body.removeChild(this.testContainer);
	});

	it('should sort table in ascending order depeding of column index passed', function(){
		var ts = new UI.TableSorter(this.testContainer.children[0]);

		// Sort by months column
		ts.sortBy(0);
		
		var rows = this.rows;
		for(var i = 1; i < rows.length; i++){
			expect(rows[i].children[0].innerHTML).toBeLessThan(rows[i - 1].children[0].innerHTML);
		}

		// Sort by money column
		ts.sortBy(1);

		for( i = 1; i < rows.length; i++){
			expect(rows[i].children[1].innerHTML).toBeLessThan(rows[i - 1].children[1].innerHTML);
		}

	});

	it('should sort table in descending order depeding of index column passed', function(){
		var ts = new UI.TableSorter(this.testContainer.children[0]);
		ts.sortBy(0, true);
		
		var rows = this.rows;
		
		for(var i = 1; i < rows.length; i++){
			expect(rows[i].children[0].innerHTML).toBeGreaterThan(rows[i - 1].children[0].innerHTML);
		}

		// Sort by money column
		ts.sortBy(1, true);

		for( i = 1; i < rows.length; i++){
			expect(rows[i].children[1].innerHTML).toBeGreaterThan(rows[i - 1].children[1].innerHTML);
		}
	});

	it('should sort column on click and changes sort direction on again click', function(){
		var ts = new UI.TableSorter(this.testContainer.children[0]);
		
		var clickEvent = document.createEvent("MouseEvents");
		clickEvent.initEvent("click", true, true);

		// Clicking on month head
		this.head[0].dispatchEvent(clickEvent);

		var rows = this.rows;
		
		for(var i = 1; i < rows.length; i++){
			expect(rows[i].children[0].innerHTML).toBeGreaterThan(rows[i - 1].children[0].innerHTML);
		}

		this.head[0].dispatchEvent(clickEvent);

		for( i = 1; i < rows.length; i++){
			expect(rows[i].children[0].innerHTML).toBeLessThan(rows[i - 1].children[0].innerHTML);
		}
	});

});