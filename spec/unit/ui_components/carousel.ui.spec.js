describe('ui.carousel',function(){

	beforeEach(function(){
		this.dummyImages = [
			{
				link: "cat_image.png",
				alt: "cat"
			},
			{
				link: "dog_image.png",
				alt: "dog"
			},
			{
				link: "monkey_image.png",
				alt: "money"
			},
			{
				link: "dinosaur_image.png",
				alt: "dinosaur"
			}
		];
		this.testContainer = document.createElement('div');
		document.body.appendChild(this.testContainer);
	});

	afterEach(function(){
		document.body.removeChild(this.testContainer);
	});

	describe('carousel with default options', function(){
		
		beforeEach(function(){
			this.carousel = new UI.Carousel(this.dummyImages);
			this.carouselEle = this.carousel.getElement();
			this.testContainer.appendChild(this.carouselEle);
			this.img = this.carouselEle.querySelector('[inShowImage]');
			this.prev = this.carouselEle.querySelector('[direction="prev"]');
			this.next = this.carouselEle.querySelector('[direction="next"]');
			this.clickEvent = document.createEvent("MouseEvents");
			this.clickEvent.initEvent("click", true, true);
		});

		it('should show first image by default', function(){
			expect(this.img.src).toContain(this.dummyImages[0].link);
			expect(this.img.alt).toEqual(this.dummyImages[0].alt);
		});

		it('setImage, bounderies check', function(){
			
			for(var i = -50; i < 50 ; i++){
				this.carousel.setImage(i);
				
				if(i < 0) {
					expect(this.img.src).toContain(this.dummyImages[0].link);
					expect(this.img.alt).toEqual(this.dummyImages[0].alt);
				}

				if( i >= 0 && i < this.dummyImages.length - 1) {
					expect(this.img.src).toContain(this.dummyImages[i].link);
					expect(this.img.alt).toEqual(this.dummyImages[i].alt);
				}

				if( i >= this.dummyImages.length) {
					expect(this.img.src).toContain(this.dummyImages[this.dummyImages.length - 1].link);
					expect(this.img.alt).toEqual(this.dummyImages[this.dummyImages.length - 1].alt);
				}
			}
			
		});

		it('on setNextImage carousel should show next image', function(){
			expect(this.img.src).toContain(this.dummyImages[0].link);
			expect(this.img.alt).toEqual(this.dummyImages[0].alt);

			this.carousel.setNextImage();

			expect(this.img.src).toContain(this.dummyImages[1].link);
			expect(this.img.alt).toEqual(this.dummyImages[1].alt);
		});

		it('checking bounderies on setNextImage and setPrevImage method', function(){
			expect(this.img.src).toContain(this.dummyImages[0].link);
			expect(this.img.alt).toEqual(this.dummyImages[0].alt);

			for(var i = 0; i < 50 ; i++){
				this.carousel.setNextImage();
			}

			expect(this.img.src).toContain(this.dummyImages[this.dummyImages.length - 1].link);
			expect(this.img.alt).toEqual(this.dummyImages[this.dummyImages.length - 1].alt);

			for( i = 0; i < 50 ; i++){
				this.carousel.setPrevImage();
			}

			expect(this.img.src).toContain(this.dummyImages[0].link);
			expect(this.img.alt).toEqual(this.dummyImages[0].alt);
		});

		it('on setPrevImage carousel should show previous image', function(){
			expect(this.img.src).toContain(this.dummyImages[0].link);
			expect(this.img.alt).toEqual(this.dummyImages[0].alt);

			this.carousel.setNextImage();
			this.carousel.setPrevImage();

			expect(this.img.src).toContain(this.dummyImages[0].link);
			expect(this.img.alt).toEqual(this.dummyImages[0].alt);
		});

		it('on click of next button, carousel should show next image', function(){
			expect(this.img.src).toContain(this.dummyImages[0].link);
			expect(this.img.alt).toEqual(this.dummyImages[0].alt);

			this.next.dispatchEvent(this.clickEvent);

			expect(this.img.src).toContain(this.dummyImages[1].link);
			expect(this.img.alt).toEqual(this.dummyImages[1].alt);
		});

		it('on click of previous button, carousel should show prevoud image', function(){
			expect(this.img.src).toContain(this.dummyImages[0].link);
			expect(this.img.alt).toEqual(this.dummyImages[0].alt);

			this.next.dispatchEvent(this.clickEvent);
			this.prev.dispatchEvent(this.clickEvent);

			expect(this.img.src).toContain(this.dummyImages[0].link);
			expect(this.img.alt).toEqual(this.dummyImages[0].alt);
		});

	});
});