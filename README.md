# Booking Assignment #

[ ![Codeship Status for thedafda/booking_assignment](https://codeship.com/projects/a7bc9c40-047e-0133-07ab-1a88c4115bd9/status?branch=master)](https://codeship.com/projects/89367)

Production Deployment Link -> https://s3-ap-southeast-1.amazonaws.com/booking.assignment/index.html


**Technology Stack**

* Native Javascript, HTML/CSS (No Framework used)
* Jasmine for unit test with Karma test runner
* Mocha and Chai for integration test with webdriver.io


**Directory Structure**

*./public* - public facing application code

*./public/scripts* - all javascript files

*./public/scripts* - all styles files

*./dist* - production(minified) output directory 

*./spec* - all tests

*./spec/unit* - all unit tests

*./spec/integration* - all integration tests

**Required Softwares**

* NodeJS and NPM

* Grunt CLI ( can be downloaded using `npm install -g grunt-cli` command)


**Commands**

Before executing any of the below commands once run `npm install` in project directory. this will download the required dependencies to the project.
  
* To start static server use `grunt connect:server:keepalive`. and go to http://localhost:9001
* To build production version - use `grunt build`. this will create minified and production ready version of whole project in `dist` directory.  
* To run unit tests -use `grunt unit-test`.  this will run unit test in chrome, phantom and firefox.
* To run integration test - use `grunt integration-test`.
* To run jshint - use `grunt jshint`.