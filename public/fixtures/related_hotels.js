window.relatedHotels = [
	{
		name: "Paragon Hotel",
		short_description: "The Paragon Hotel is in Abu Dhabi, less than 325 ft from the Madinat Zayed Shopping Center. It has spacious air-conditioned rooms with city views and free Wi-Fi",
		rating: 4,
		photo: "imgs/hotels/hotel_1.jpg",
		details_link: "" 
	},
	{
		name: "Crowne Plaza",
		short_description: "Set along the Links Championship Golf Course on Yas Island, this luxurious hotel features a 25 m outdoor pool with furnished terrace. It offers spacious guest rooms with balconies, and free Wi-Fi.",
		rating: 4,
		photo: "imgs/hotels/hotel_2.jpg",
		details_link: ""
	},
	{
		name: "Trianon Hotel",
		short_description: "All accommodations at Trianon feature a warm and elegant décor. Each has a small sitting area with a flat-screen TV, a wardrobe and a minibar. The suite includes a coffee table for 4 people",
		rating: 2,
		photo: "imgs/hotels/hotel_3.jpg",
		details_link: ""
	},
	{
		name: "Olivia Hotel",
		short_description: "Each has a small sitting area with a flat-screen TV, a wardrobe and a minibar. The suite includes a coffee table for 4 people",
		rating: 2,
		photo: "imgs/hotels/hotel_4.jpg",
		details_link: ""
	}
];