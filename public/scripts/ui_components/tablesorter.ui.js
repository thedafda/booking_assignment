(function(w){
	'use strict';
	/**
	 * TableSorter UI Component
	 * @param {Object} model : Data source for TableSorter components
	 * @param {Object} options : configuration for creating TableSorter
	 * options : {
	 * 		container: {DOM Element} : Element container 
	 * 		render : {Function} : Renderer function, it should return template string to render item
	 * }
	 */
	function TableSorter(container, options){
		this.options = options || {};
		this.container = container;
		this.thead = this.container.querySelector('thead');
		this.tbody = this.container.querySelector('tbody');
		this.theadTr = this.thead.querySelector('tr');
		
		this.valueExtractors = this.options.valueExtractors || [];
		this.excludeRow = this.options.exclude || [];
		this.setValueExtractors();
		this.bindEvents();
	}

	TableSorter.prototype.setValueExtractors = function(){
		var col = this.theadTr.children.length || 0, i;
		function defaultExtractor(node){
			return node.innerHTML;
		}
		this.valueExtractor = [];
		for(i = 0; i < col; i++){
			this.valueExtractor[i] = (this.options.valueExtractors && this.options.valueExtractors[i]) || defaultExtractor;
		}
	};

	TableSorter.prototype.sortBy = function(colNumber, direction){
		var children = this.tbody.children, i, earr = [], that = this;
		if(this.excludeRow.indexOf(colNumber) > -1) {
			return;
		}
		for(i = 0 ; i < children.length; i++){
			earr.push(children[i]);
		}
		earr.sort(function(a, b){
			var avalue = that.valueExtractor[colNumber](a.children[colNumber]),
				bvalue = that.valueExtractor[colNumber](b.children[colNumber]);
			if(avalue === bvalue) {
				return 0;
			}
			return direction ? (avalue > bvalue ? 1 : -1) : (avalue > bvalue ? -1 : 1);
		});
		for(i = 0; i < earr.length; i++){
			this.tbody.appendChild(earr[i]);
		}
		if(this.currentSorter){
			w.utils.removeClass(this.currentSorter, 'sortUp');
			w.utils.removeClass(this.currentSorter, 'sortDown');	
		}
		this.currentSorter = this.theadTr.children[colNumber];
		w.utils.addClass(this.currentSorter, direction ? 'sortUp' : 'sortDown');
		that.currentSortOrder = direction;
	};

	TableSorter.prototype.getElement = function(){
		return this.container;
	};

	/**
	 * bindEvents
	 * function to bind all DOM events,
	 * Event Deligation
	 */
	TableSorter.prototype.bindEvents = function() {
		var that = this;
		this.theadTr.addEventListener('click', function(e){
			var target = e.target;
			if(target.tagName.toLowerCase() === 'th'){
				that.currentSortOrder = !that.currentSortOrder;
				that.sortBy(target.cellIndex, that.currentSortOrder);
			}
		});
	};

	/**
	 * removeEvents
	 * function to unbind all DOM events
	 */
	TableSorter.prototype.removeEvents = function() {

	};

	TableSorter.prototype.destroy = function() {
		this.removeEvents();
	};

	w.UI = w.UI || {};
	w.UI.TableSorter = TableSorter;
}(window));