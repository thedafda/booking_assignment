(function(w){
	'use strict';
	/**
	 * Carousel UI Component
	 * @param {Object} model : Data source for Carousel components
	 * @param {Object} options : configuration for creating Carousel
	 * options : {
	 * 		container: {DOM Element} : Element container 
	 * 		render : {Function} : Renderer function, it should return template string to render item
	 * }
	 */
	function Carousel(model, options){
		this.options = options || {};
		this.model = model;
		this.container = this.options.container || document.createElement('div');
		this.render();
		this.bindEvents();
		this.setImage(this.options.startIndex || 0);
		if(this.options.startTimer) {
			this.startTimer();
		}
	}

	Carousel.prototype.HTMLTmplts = {
		container: [
			'<div class="carousel">',
				'<div class="navigation prev" direction="prev">&#9664;</div>',
				'<img inShowImage class="easeShow inShowImage"/>',
				'<div class="navigation next" direction="next">&#9654;</div>',
			'</div>'
		].join(''),
	};

	Carousel.prototype.getElement = function(){
		return this.container.children[0];
	};

	Carousel.prototype.render = function(){
		this.container = this.container || document.createElement('div');
		this.container.innerHTML = this.HTMLTmplts.container;
		this.inShowImage = this.container.querySelector('[inShowImage]');
	};

	Carousel.prototype.setImage = function(index){
		if(this.model[index]){
			this.inShowImage.src = this.model[index].link;
			this.inShowImage.alt = this.model[index].alt || '';
			this.currentIndex = index;
			if(this.options.imageChanged){
				this.options.imageChanged.call(this, index, this.model[index]);
			}
		}
	};

	Carousel.prototype.setNextImage = function(goRound){
		if(goRound && this.currentIndex >= this.model.length - 1){
			this.currentIndex = -1;
		}
		this.setImage(this.currentIndex + 1);
	};

	Carousel.prototype.setPrevImage = function(goRound){
		if(goRound && this.currentIndex === 0){
			this.currentIndex = this.model.length;
		}
		this.setImage(this.currentIndex - 1);
	};

	Carousel.prototype.setRoundImage = function(){
		
	};

	Carousel.prototype.startTimer = function(){
		var that = this;
		this.timer = setInterval(function(){
			if(that.currentIndex >= that.model.length - 1){
				that.currentIndex = -1;
			}
			that.setNextImage();
		}, this.options.startInterval || 4000);
	};

	Carousel.prototype.stopTimer = function(){
		clearInterval(this.timer);
	};

	Carousel.prototype.resetTimer = function(){
		this.stopTimer();
		this.startTimer();
	};

	/**
	 * bindEvents
	 * function to bind all DOM events,
	 * Event Deligation
	 */
	Carousel.prototype.bindEvents = function() {
		var that = this;
		this.container.children[0].addEventListener('click', function(e){
			var target = e.target;
			if(target.hasAttribute('direction')){
				var value = target.getAttribute('direction');
				if(value === 'prev'){
					that.setPrevImage(true);
					that.resetTimer();
				} else if(value === 'next') {
					that.setNextImage(true);
					that.resetTimer();
				}
			}
		});
	};

	/**
	 * removeEvents
	 * function to unbind all DOM events
	 */
	Carousel.prototype.removeEvents = function() {

	};

	Carousel.prototype.destroy = function() {
		this.removeEvents();
	};

	w.UI =  w.UI || {};
	w.UI.Carousel = Carousel;
}(window));