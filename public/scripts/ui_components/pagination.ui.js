(function(w){
	'use strict';
	/**
	 * Pagination UI Component
	 * @param {Object} model : Data source for Pagination components
	 * @param {Object} options : configuration for creating pagination
	 * options : {
	 * 		container: {DOM Element} : Element container 
	 * 		render : {Function} : Renderer function, it should return template string to render item
	 * }
	 */
	function Pagination(model, options){
		options = options || {};
		this.model = model;
		this.render = options.render;
		this.container = options.container;
		this.pagingContainer = options.pagingContainer;
		this._render();
		this.bindEvents();
		this.renderPage(0);
	}

	Pagination.prototype.HTMLTmplts = {
		pContainer: [
			'<div class="pagination">',
				'<div value="start" class="static">Start</div>',
				'<div value="prev" class="static">Prev</div>',
				'<div pages> </div>',
				'<div value="next" class="static">Next</div>',
				'<div value="end" class="static">End</div>',
			'</div>'
		].join(''),
	};

	Pagination.prototype._render = function(){
		var total, i, tmplt = '', current;
		this.pagingContainer.innerHTML = this.HTMLTmplts.pContainer;
		total = this.model.getTotalPages();
		current = this.model.getCurrentPageNumber();
		for(i = 0; i < total; i++) {
			tmplt += '<div value="{value}" class="{classes}">{displayValue}</div>'.supplant({
				value: i,
				displayValue: i + 1,
				classes: (current === i) ? 'active' : ''
			});
		}
		this.pagingContainer.querySelector('[pages]').innerHTML = tmplt;
	};

	/**
	 * bindEvents
	 * function to bind all DOM events,
	 * Event Deligation
	 */
	Pagination.prototype.bindEvents = function() {
		var that = this;
		this.pagingContainer.addEventListener('click', function(e){
			var target = e.target;
			if(target.hasAttribute('value')){
				var value = target.getAttribute('value');
				if(value){
					that.renderPage(value);
				}
			}
		});
	};

	Pagination.prototype.renderPage = function(pageNumber) {
		var staticState = [ 'start' , 'next' ,'prev', 'end'],
			page, tmplt = '', that = this, pagesContainer, p;
		if(staticState.indexOf(pageNumber) > -1){
			if(pageNumber === 'start') {
				page = this.model.getFirstPage();
			} else if(pageNumber === 'prev') {
				page = this.model.getPreviousPage();
			} else if(pageNumber === 'next') {
				page = this.model.getNextPage();
			} else if(pageNumber === 'end') {
				page = this.model.getLastPage();
			}
		} else if(this.model.isValidPageNumber(Number(pageNumber))){
			page = this.model.getPage(pageNumber);
		}

		if(page){
			page.forEach(function(item){
				tmplt += that.render(item);
			});
			this.container.innerHTML = tmplt;

			// Updating active class
			p = this.pagingContainer;
			utils.removeClass(p.querySelector('.active'), 'active');
			utils.addClass(p.querySelector('[value="' + this.model.getCurrentPageNumber() + '"]'), 'active');
		}
	};

	/**
	 * removeEvents
	 * function to unbind all DOM events
	 */
	Pagination.prototype.removeEvents = function() {

	};

	w.UI =  w.UI || {};
	w.UI.Pagination = Pagination;
}(window));