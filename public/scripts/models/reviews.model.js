(function(w){
	'use strict';
	/**
	 * Reviews Model : Data Source for reviews service
	 * @param {Array} reviewsArray : Array of reviews
	 * @param {Object} options : configuration for creating pagination
	 * options : {
	 *   	itemsPerPage: {Number}  : Number of items per page,
	 *   	startPage: {Number} : Staring page number default is first page
	 * }
	 */
	function ReviewsModel(reviewsArray, options){
		options = options || {};
		this.reviewsArray = reviewsArray;
		this.reviews = reviewsArray || [];
		this.total = reviewsArray.length;
		this.itemsPerPage = options.itemsPerPage || 5; // Dafault Page Size is 5
		this.noOfPages = Math.ceil(this.total / this.itemsPerPage);
		this.currentPage = options.startPage || 0; // default first page
	}

	/*ReviewsModel.prototype.sortBy = function(type, direction){
		if(type === 'score'){
			this.reviews = this.reviews.sort(function(a,b){
				return direction ? a.score - b.score : b.score - a.score;
			});	
		} else if(type === 'original') {
			this.reviews = this.reviewsArray;
		}
	};*/

	ReviewsModel.prototype.getCurrentPageNumber = function(){
		return this.currentPage;
	};

	ReviewsModel.prototype.getTotalPages = function(){
		return this.noOfPages;
	};

	ReviewsModel.prototype.getTotalReviews = function(){
		return this.total;
	};

	ReviewsModel.prototype.isValidPageNumber = function(pageNumber){
		if(pageNumber > this.noOfPages - 1  ||  pageNumber < 0 || isNaN(pageNumber)) {
			return;
		}
		return true;
	};

	ReviewsModel.prototype.getPage = function(pageNumber){
		if(!this.isValidPageNumber(pageNumber)){
			return;
		}
		this.currentPage = Number(pageNumber);
		var startIndex = this.itemsPerPage * (this.currentPage);
		var endIndex = startIndex + this.itemsPerPage;
		return this.reviews.slice(startIndex, endIndex);
	};

	ReviewsModel.prototype.getNextPage = function(){
		return this.getPage(this.currentPage + 1);
	};

	ReviewsModel.prototype.getPreviousPage = function(){
		return this.getPage(this.currentPage - 1);
	};

	ReviewsModel.prototype.getFirstPage = function(){
		return this.getPage(0);
	};

	ReviewsModel.prototype.getLastPage = function(){
		return this.getPage(this.noOfPages - 1);
	};

	w.ReviewsModel = ReviewsModel;
}(window));