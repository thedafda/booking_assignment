(function (w, document) {
	'use strict';
	
	var classList = document.documentElement.classList;
	
	function hasClassFn(ele, className) {
		if (classList) {
			return ele && ele.classList.contains(className);
		} else {
			return ele.className.indexOf(" " + className + " ") > -1 ? true : false;
		}
	}
	
	function removeClassFn(ele, className) {
		if (hasClassFn(ele, className)) {
			if (classList) {
				ele.classList.remove(className);
			} else {
				ele.className.replace(className, '');
			}
		}
	}
	
	function addClassFn(ele, className) {
		if (!hasClassFn(ele, className)) {
			if (classList) {
				ele.classList.add(className);
			} else {
				ele.className = ele.className + " " + className;
			}
		}
	}

	// DOM Helper Unitlity functions
	w.utils = {
		hasClass : hasClassFn,
		removeClass: removeClassFn,
		addClass : addClassFn
	};

	// Polyfill for addEventListerner (IE8)
	if(!document.addEventListener){
		w.Element.prototype.addEventListener = function(eventname, fn){
			var that = this;
			that.attachEvent('on' + eventname,  function(e){
				e = e || w.event;
				fn.call(that, e);
			});
		};
	}

	if(!document.removeEventListener){
		w.Element.prototype.removeEventListener = function(eventname, fn){
			var that = this;
			that.detachEvent('on' + eventname,  function(e){
				e = e || w.event;
				fn.call(that, e);
			});
		};
	}


}(window, document));