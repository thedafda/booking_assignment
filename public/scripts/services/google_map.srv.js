(function(w){
  'use strict';
  
  function GoogleMap(containerEle, mapOptions){
    mapOptions = mapOptions || {};
    mapOptions.center = mapOptions.center || {
      lat: 17.3700,
      lng: 78.4800
    };
    mapOptions.zoom = mapOptions.zoom || 13;
    this.extraDetail = mapOptions.extraDetail;
    this.center = new google.maps.LatLng(mapOptions.center.lat,mapOptions.center.lng);
    this.map = new google.maps.Map(containerEle, mapOptions);
    this.placeService = new google.maps.places.PlacesService(this.map);
    this.infoWindow = this.createInfoWindow({
      content: '<h1>' + this.extraDetail.name + '</h1>' + this.extraDetail.address,
      position: this.center
    });
    
  }

  GoogleMap.prototype.createMarker = function(markerOptions, eventListenersObject) {
    var dMarkerOptions = { map: this.map };
    w._.extend(dMarkerOptions, markerOptions || {});
    var marker = new google.maps.Marker(dMarkerOptions);

    if(eventListenersObject){
      Object.keys(eventListenersObject).forEach(function(eventName){
        google.maps.event.addListener(marker, eventName, eventListenersObject[eventName]);
      });
    }
    return marker;
  };

  GoogleMap.prototype.createMarkers = function(markerArray){
    (markerArray || []).forEach(function(obj){
      this.createMarker(obj.maker, obj.events );
    }.bind(this));
  };

  GoogleMap.prototype.createInfoWindow = function(optionsInfoWindow){
    return new google.maps.InfoWindow(optionsInfoWindow);
  };

  GoogleMap.prototype.searchNearByPlaces = function(searchOptions, successCallback, errorCallback){
    searchOptions = searchOptions || {};
    this.placeService.nearbySearch(searchOptions, function(results, status){
      if (status != google.maps.places.PlacesServiceStatus.OK) {
        errorCallback(status);
        return;
      }
      successCallback(results);
    });
  };

  GoogleMap.prototype.searchNearByPlacesToCenter = function(searchOptions, successCallback, errorCallback){
    searchOptions = searchOptions || {};
    searchOptions.location = this.center;
    searchOptions.radius = this.radius || 800;
    this.searchNearByPlaces(searchOptions, successCallback, errorCallback);
  };

  GoogleMap.prototype.getDetailOfPlace = function(place, successCallback, errorCallback){
    this.placeService.getDetails(place, function(result, status) {
      if (status != google.maps.places.PlacesServiceStatus.OK) {
        errorCallback(status);
        return;
      }
      successCallback(result);
    });
  };

  GoogleMap.prototype.renderNearByPlaces = function(){
    var that = this;

    that.searchNearByPlacesToCenter({}, function(places){
      var markerArray = (places || []).map(function(place){
        return {
          maker :{
            position: place.geometry.location,
            icon: {
              url: place.icon,
              scaledSize: new google.maps.Size(23, 23),
              origin: new google.maps.Point(0,0),
              anchor: new google.maps.Point(0, 0)
            }
          },
          events:  {
            click: function(){
              that.getDetailOfPlace(place, function(placeDetail){
                var photoshtml = placeDetail.photos && placeDetail.photos.map(function(photo){
                   return '<img class="gmap-place-images" src="' + photo.getUrl({ maxWidth: 100, maxHeight: 100}) + '"/>';
                }).join('') || '';

                var tmplt = [
                    '<h3>' + placeDetail.name + '</h3>',
                    '<div>' + placeDetail.formatted_address + '</div>',
                    photoshtml
                ].join('');
                console.log(tmplt);
                that.infoWindow.setContent(tmplt);
                that.infoWindow.open(that.map, this);
              }.bind(this));
            }
          }
        };
      });
      that.createMarkers(markerArray);
    }, function(){
      console.log('Error while searching near by places');
    });
  };

  GoogleMap.prototype.renderCenter = function(openWindow){
    var that = this;

    this.centerMaker = this.createMarker({
      position: this.center,
      icon: 'imgs/hotel_marker.png'  
    },
    {
      click: function(){
        var tmplt = '<h1>' + that.extraDetail.name + '</h1>' + that.extraDetail.address;
        that.infoWindow.setContent(tmplt);
        that.infoWindow.open(that.map, this);
      }
    });
    if(openWindow) {
      this.infoWindow.open(this.map, this.centerMaker);
    }
  };

  w.GoogleMap = GoogleMap; 
})(window);
  