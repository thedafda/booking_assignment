document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  /*######################## Carousel #######################*/
  

  function extractor(container){
    var arr = [];
    for (var i = 0; i < container.children.length; i++) {
        var href = container.children[i].children[0].getAttribute('href');
        var alt = container.children[i].children[0].children[0].getAttribute('alt');
        arr.push({
          link: href,
          alt: alt
        });
    }
    return arr;
  }
  var photoList = document.getElementById('photo-list');
  
  var prevActive = photoList.children[0];
  utils.addClass(prevActive, 'active');

  var carousel = new UI.Carousel(extractor(photoList), {
    startTimer: true,
    imageChanged: function(index){
      if(prevActive){
        utils.removeClass(prevActive, 'active');
      }
      prevActive = photoList.children[index];
      utils.addClass(prevActive, 'active');
    }
  });

  photoList.addEventListener('click', function(e){
    e.preventDefault();
    var targetLi;
    var tagname = e.target.tagName.toLowerCase();
    if(tagname === 'li'){
      targetLi = e.target;
    } else if(tagname === 'a'){
      targetLi = e.target.parentNode;
    } else if(tagname === 'img') {
      targetLi = e.target.parentNode.parentNode;
    }
    var index = Array.prototype.indexOf.call(photoList.children, targetLi);
    if(index > -1){
      carousel.setImage(index);
      carousel.resetTimer();
    }
    return false;
  });

  document.getElementById('carousel-container').appendChild(carousel.getElement());


  /*######################## Facilities #######################*/

  var list = document.querySelectorAll('.show-rating') || [];
  
  Array.prototype.forEach.call(list, function(item){
    var random = Math.floor((Math.random() * 100) + 1);
    item.style.width = random + '%';
    //utils.addClass(item, active);
  });             

  /*######################## Table Sorter #######################*/
  
  var roomTabelContainer = document.getElementById('rooms-table');
  var roomsTable = new UI.TableSorter(roomTabelContainer, {
    valueExtractors: [
      undefined,
      function(node){
        return Number(node.innerHTML);
      },
      function(node){
        return Number(node.innerHTML.replace('€',''));
      }
    ],
    exclude: [3]
  });

  // default sortby occupancy
  roomsTable.sortBy(1, true);

  // trick to not show rearranging;
  utils.removeClass(roomTabelContainer, 'visibility-hidden');

  /*######################## Location #######################*/

  function initialize() {
    var googMap = new window.GoogleMap(document.getElementById('map-canvas'), {
      center: {
        lat: -34.611906,
        lng:-58.408838
      },
      extraDetail: {
        name: window.hotel.name,
        address: window.hotel.address
      }
    });
    googMap.renderCenter(true);
    googMap.renderNearByPlaces();
  }

  google.maps.event.addDomListener(window, 'load', initialize); 


  /*######################## Reviews ########################*/

  /* Initilizing Reviews Model */
  var reviewmodel = new window.ReviewsModel(hotel.reviews);
  var totalReviews = reviewmodel.getTotalReviews();

  //reviewmodel.sortBy('score',true);
  //reviewmodel.sortBy('original');

  /* Updating total reviews count */
  document.getElementById('total-reviews').innerHTML = totalReviews ? '(' + totalReviews + ')' : '';

  var reviewRowTemplate = [
  	'<li class="one_review">',
  	'<strong class="review_score">{score}</strong>',
  	'<blockquote class="review_content">',
      	'{content}',
      	'<cite>{name}</cite>',
  	'</blockquote>',
  '</li>'
  ].join('');

  /* Initializing Rewins UI component */
  var reviewPagination = new window.UI.Pagination(reviewmodel, {
  		container: document.getElementById('reviews-container'),
  		pagingContainer: document.getElementById('reviews-paging-container'),
  		render: function(rowObject){
  			rowObject.name = rowObject.user_info.name;
  			return reviewRowTemplate.supplant(rowObject);
  		}
  });


   /*######################## Related Hotels ########################*/
   var tmplt = '';
   window.relatedHotels.forEach(function(obj){
      var p = [
        '<div class="related-hotel">',
          '<a href="#tohotel_{name}" class="name"><h2> {name} </h2></a>',
          '<img src="{photo}" alt="{name}"/>',
          '<div class="rating">Rating:  {rating} / 5 </div>',
          '<div class="desc"> {short_description} </div>',
          '<div class="more_link"><a href="#tohotel_{name}"> more info > </a></div>',
        '</div>'
      ].join('');
      tmplt += p.supplant(obj);
   });
   document.getElementById('related-hotels-container').innerHTML = tmplt;

}, false);