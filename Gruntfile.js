module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      build: {
        src: [
          //Fixtuers
          'public/fixtures/**/*.js',
          // Core
          'public/scripts/utils/helpers.js',
          'public/scripts/utils/dom_helpers.js',
          //Service
          'public/scripts/services/google_map.srv.js',
          // Models
          'public/scripts/models/reviews.model.js',
          // UI Components
          'public/scripts/ui_components/*.ui.js',
          // Main
          'public/scripts/main.js'
        ],
        dest: 'dist/main.min.js'
      }
    },
    cssmin:{
      build: {
        files: {
          'dist/styles.min.css': [
            'public/styles/**/*.css'
          ]
        }
      }
    },
    copy: {
      build: {
        files: [
          {
            expand: true,
            cwd: 'public',
            src: ['imgs/**'],
            dest: 'dist'
          }
        ]
      }
    },
    processhtml: {
      build: {
        files: {
          'dist/index.html': ['public/index.html']
        }
      }
    },
    jshint: {
      all: [
        'public/**/*.js',
        'spec/**/*.js'
      ]
    },
    clean:{
      build: {
        src: ["dist"]
      },
      zip: {
        src: [
          "dist.zip",
          "project.zip",
        ]
      }
    },
    karma: {
      unit: {
        configFile: 'karma.conf.js'
      }
    },
    webdriver: {
      chrome: {
          options:{
            desiredCapabilities: {
                browserName: 'chrome'
            }
          },
          tests: ['spec/integration/**/*.js']
      },
      firefox: {
          options:{
            desiredCapabilities: {
                browserName: 'firefox'
            }
          },
          tests: ['spec/integration/**/*.js']
      }
    },
    connect: {
      server: {
        options: {
          port: 9001,
          base: './dist'
        }
      }
    },
    compress: {
      dist: {
        options: {
          archive: 'dist.zip'
        },
        files: [
          {src: ['dist/**/*' , '!./node_modules'], dest: './'}
        ]
      },
      project: {
        options: {
          archive: 'project.zip'
        },
        files: [
          {src: ['**/*' , '!**/node_modules/**', '!**/dist/**' ,'!dist.zip', '!project.zip'], dest: './'}
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-processhtml');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-webdriver');

  
  grunt.registerTask('build', ['jshint', 'clean', 'uglify', 'cssmin', 'processhtml', 'copy']);
  grunt.registerTask('unit-test', ['jshint', 'karma']);
  grunt.registerTask('integration-test', ['build', 'connect', 'webdriver']);
  grunt.registerTask('default', ['build']);
};